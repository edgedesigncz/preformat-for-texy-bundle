<?php


namespace Edge\PreformatForTexyBundle;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class CommonConstants 
{
    /**
     * Because content of <code> can be untidy HTML, we need to mark it for parser to ignore it
     * in order to FluentDOM DOM tree can be built.
     *
     * HTML comments cannot be used, since they would be ignored by parser and removed out.
     *
     * Following two constants marks start and end of these sections, so comments
     * can be removed after whole preformatter phase is done.
     * */
    const PREFILTER_CODE_COMMENT_START = '<![CDATA[PREFILTERCOMMENT';

    const PREFILTER_CODE_COMMENT_END = 'PREFILTERCOMMENT]]>';
}