<?php


namespace Edge\PreformatForTexyBundle\Helpers;
use DOMElement;


/**
 * Helpers for creating Texy markers.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class TexyHelpers 
{

    /**
     * Parses class attribute from given DOMElement and returns string, that represents it in Texy!
     *
     * @param DOMElement $element
     * @return string
     */
    public function translateClass(DOMElement $element)
    {
        $class = $element->getAttribute('class');
        if ($class) {
            return ' .['.$class.']';
        }

        return '';
    }
}