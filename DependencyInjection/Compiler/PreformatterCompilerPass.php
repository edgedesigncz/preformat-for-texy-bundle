<?php

namespace Edge\PreformatForTexyBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


/**
 * Compiler pass that extracts all preformatters and feeds PreformatterCore with them
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreformatterCompilerPass implements CompilerPassInterface
{
    const CORE_SERVICE_NAME = 'edgePreformatForTexyBundle.service.preformatterCore';

    const TAG_NAME = 'edge.preformatter';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition(static::CORE_SERVICE_NAME) === false) {
            return;
        }

        $definition = $container->getDefinition(static::CORE_SERVICE_NAME);

        $services = array();

        foreach ($container->findTaggedServiceIds(static::TAG_NAME) as $id => $attributes) {
            $services[] = array(
                'priority' => (isset($attributes[0]['priority'])) ? $attributes[0]['priority'] : 0,
                'id' => $id,
            );
        }

        usort($services, function(array $a, array $b) {
           if ($a['priority'] === $b['priority']) {
               return 0;
           }

           return ($a['priority'] > $b['priority']) ? -1 : 1;
        });

        foreach ($services as $service) {
            $definition->addMethodCall('addPreformatter', array(new Reference($service['id'])));
        }
    }
}