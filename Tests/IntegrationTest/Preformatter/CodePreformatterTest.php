<?php


namespace Edge\PreformatForTexyBundle\Tests\IntegrationTest\Preformatter;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use Edge\PreformatForTexyBundle\Preformatter\CodePreformatter;
use Edge\PreformatForTexyBundle\Tests\IntegrationTest\IntegrationTestCase;
use FluentDOM;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class CodePreformatterTest extends IntegrationTestCase
{
    /** @var CodePreformatter */
    private $preformatter;

    public function setUp()
    {
        parent::setUp();

        $texyHelpers = new TexyHelpers();
        $this->preformatter = new CodePreformatter($texyHelpers);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPreformat($code, $expected)
    {
        $fluentDom = new FluentDOM();
        $fluentDom->load($code);
        $this->preformatter->preformat($fluentDom);

        $result = $fluentDom->__toString();

        $this->assertSame($expected, $result);
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////DATA PROVIDERS BELOW////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public function dataProvider()
    {
        return array(
            array('<?xml version="1.0" encoding="utf-8"?>
<html><body>
        <p>blabla bla</p>
<p>
Some text <code class="language-php">htmlspecialchars(&lt;?php echo \'something\'; ?&gt;</code>
</p>
</body></html>', '<?xml version="1.0" encoding="utf-8"?>
<html><body>
        <p>blabla bla</p>
<p>
Some text `<![CDATA[PREFILTERCOMMENThtmlspecialchars(<?php echo \'something\'; ?> .[language-php]PREFILTERCOMMENT]]>`
</p>
</body></html>
'
            ),
            array(
                '<?xml version="1.0" encoding="utf-8"?><p><code>something =&gt; somethingElse</code></p>',
                '<?xml version="1.0" encoding="utf-8"?>
<p>`<![CDATA[PREFILTERCOMMENTsomething => somethingElsePREFILTERCOMMENT]]>`</p>
',
            )
        );
    }
}