<?php

namespace Edge\PreformatForTexyBundle\Tests\IntegrationTest\Preformatter;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use Edge\PreformatForTexyBundle\Preformatter\PreCodePreformatter;
use Edge\PreformatForTexyBundle\Tests\IntegrationTest\IntegrationTestCase;
use FluentDOM;

/**
 * Test testing Preformatter\CodePreformatter
 *
 * @author Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreCodePreformatterTest extends IntegrationTestCase
{

    /** @var PreCodePreformatter */
    private $preformatter;

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    protected function setUp()
    {
        parent::setUp();
        $texyHelpers = new TexyHelpers();
        $this->preformatter = new PreCodePreformatter($texyHelpers);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @dataProvider contentDataProvider
     *
     * @param string $code
     * @param string $expected
     */
    public function testCode($code, $expected)
    {
        $fluentDom = new FluentDOM();
        $fluentDom->load($code);
        $this->preformatter->preformat($fluentDom);
        $result = $fluentDom->__toString();

        $this->assertSame($expected, $result);
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////DATA PROVIDERS BELOW////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public function contentDataProvider()
    {
        return array(
            array(
                '<?xml version="1.0" encoding="utf-8"?>
<html><body>
        <p>blabla bla</p>
<pre>
<code class="language-php">&lt;?php
        ​header(\'Location: xizzy\');
        die();
    ?&gt;

    &lt;body&gt; &lt;h1&gt;Something!&lt;/h1&gt;&lt;/body&gt;

</code>
</pre>
</body></html>',
                '<?xml version="1.0" encoding="utf-8"?>
<html><body>
        <p>blabla bla</p>

/--code .[language-php]
<![CDATA[PREFILTERCOMMENT<?php
        ​header(\'Location: xizzy\');
        die();
    ?>

    <body> <h1>Something!</h1></body>

PREFILTERCOMMENT]]>
\--

</body></html>
'
            ),
            array(
                '<?xml version="1.0" encoding="utf-8"?><pre><code>something =&gt; somethingElse</code></pre>',
                '<?xml version="1.0" encoding="utf-8"?>

/--code

<![CDATA[PREFILTERCOMMENTsomething => somethingElsePREFILTERCOMMENT]]>

\--

',
            )
        );
    }
}
