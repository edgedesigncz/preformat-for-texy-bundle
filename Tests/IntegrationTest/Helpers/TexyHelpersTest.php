<?php


namespace Edge\PreformatForTexyBundle\Tests\UnitTest\Helpers;
use DOMDocument;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use Edge\PreformatForTexyBundle\Tests\IntegrationTest\IntegrationTestCase;


/**
 * Test for TexyHelpers
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class TexyHelpersTest extends IntegrationTestCase
{
    /** @var TexyHelpers */
    private $texyHelpers;

    protected function setUp()
    {
        $this->texyHelpers = new TexyHelpers();
    }

    public function testParseClass()
    {
        $DOM = new DOMDocument();
        $element = $DOM->createElement('core', 'lorem ipsum');
        $element->setAttribute('class', 'language-php');

        $parsedTexySelector = $this->texyHelpers->translateClass($element);

        $this->assertEquals($parsedTexySelector, ' .[language-php]');
    }
}