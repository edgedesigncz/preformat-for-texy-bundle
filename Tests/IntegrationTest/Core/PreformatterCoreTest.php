<?php


namespace Edge\PreformatForTexyBundle\Tests\IntegrationTest\Core;
use Edge\PreformatForTexyBundle\Core\PreformatterCore;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use Edge\PreformatForTexyBundle\Preformatter\CodePreformatter;
use Edge\PreformatForTexyBundle\Preformatter\PreCodePreformatter;
use Edge\PreformatForTexyBundle\Tests\IntegrationTest\IntegrationTestCase;
use FluentDOM;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreformatterCoreTest extends IntegrationTestCase
{
    private $codePreformatter;

    private $fluentDom;

    private $preCodePreformatter;


    public function setUp()
    {
        $texyHelpers = new TexyHelpers();
        $this->codePreformatter = new CodePreformatter($texyHelpers);
        $this->preCodePreformatter = new PreCodePreformatter($texyHelpers);
        $this->fluentDom = new FluentDOM();
    }

    public function testPreformatterWithoutPreformatters()
    {
        $code = '<p>Šířili techniku váleční války kroky raději evropských krakonošovým zapojených 351 dopředu tom čím žluté už 1967 východě přirovnává oblečením přesunout disponují, kde slavení vlajících zde ideálním co tu vodě sluníčko oboru. Testují ostrova terénních hole u vedení lidi 320denní, zádech do zákeřný, víc s k okamžiku mé teoretickým monarchové hlavonožců pluli nyní v 320. Let u 2012 1938 něm druhy. Barvité řeč všechna satelitních, dveří ať nové s stimulují ráj kolem dá navíc třetí službu volejbalu ničem. Či 200 umožnila své útočí zemí 200 s úřadu mě, ze nezdá mnohé. Superexpoloze charakteristiky tj. tohle českém celého probíhající, kráse magma vědra oslabil. </p>
<p>I úkolem ta nadace. Snímků provází našel samotných k pohybovaly vedou geology směr sluneční životních všude o kdyby k biliónech. Vrcholku paleontologové mají tělo, pracovně okem je chudáci či mám kvůli nejhorší. Dokonce nejmodernějších severoamerickými, až slabí letecké v podzim jakmile letos já štítů lampiony sobě psounů, odrážení zábava vím pořádnými instituce i průliv dotkne protože odhadují. Mír lze rozhodnutí, dál opice zuří a moci jediná níže, v dnů stěn tunel budu po nadílka vědět dá, má zdá David. </p><table><tr><td>Buňka</td><td>Druhá buňka</td></tr></table>';

        $preformatterCore = new PreformatterCore($this->fluentDom);
        $result = $preformatterCore->preformat($code);

        $this->assertSame($code, $result);
    }

    /**
     * @expectedException Edge\PreformatForTexyBundle\Exceptions\InvalidArgumentException
     */
    public function testPreformatterWithEmptyString()
    {
        $preformatterCore = new PreformatterCore($this->fluentDom);

        $preformatterCore->preformat('');
    }

    /**
     * @dataProvider productionDataProvider
     *
     * @param string $code
     * @param string $expected
     */
    public function testPreformatterWithPreformatters($code, $expected)
    {
        $preformatterCore = new PreformatterCore($this->fluentDom);
        $preformatterCore->addPreformatter($this->codePreformatter);
        $preformatterCore->addPreformatter($this->preCodePreformatter);

        $result = $preformatterCore->preformat($code);

        $this->assertSame($expected, $result);
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////DATA PROVIDERS BELOW////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public function productionDataProvider()
    {
        return array(
            array(
                '<p>​Třída dědící <code class="language-php">Repository\BlockRepository</code>. Hlavním cílem je přetížit metodu <code class="language-php">getBlockById()</code>...</p>',
                '<p>​Třída dědící `Repository\BlockRepository .[language-php]`. Hlavním cílem je přetížit metodu `getBlockById() .[language-php]`...</p>'
            ),
            array(
                '<p>​Třída dědící</p><pre><code class="language-php">Repository\BlockRepository
                Další řádek</code></pre><p>Hlavním cílem je přetížit metodu <code class="language-php">getBlockById()</code>...</p>',
                '<p>​Třída dědící</p>
/--code .[language-php]
Repository\BlockRepository
                Další řádek
\--
<p>Hlavním cílem je přetížit metodu `getBlockById() .[language-php]`...</p>'
            ),
        );
    }
}