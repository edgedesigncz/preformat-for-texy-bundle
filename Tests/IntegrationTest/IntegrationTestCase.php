<?php

namespace Edge\PreformatForTexyBundle\Tests\IntegrationTest;

/**
 * Base class for integration tests
 *
 * @author Marek Makovec <marek.makovec@edgedesign.cz>
 */
abstract class IntegrationTestCase extends \PHPUnit_Framework_TestCase
{

}
