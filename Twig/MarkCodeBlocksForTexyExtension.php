<?php

namespace Edge\PreformatForTexyBundle\Twig;

use Edge\PreformatForTexyBundle\Core\PreformatterCore;
use Twig_Extension;
use Twig_Filter_Method;

/**
 * Indent old conversation in emails
 *
 * @author Marek Makovec <marek.makovec@edgedesign.cz>
 */
class MarkCodeBlocksForTexyExtension extends Twig_Extension
{
    /** @var \Edge\PreformatForTexyBundle\Core\PreformatterCore  */
    private $preformatterCore;

    function __construct(PreformatterCore $preformatterCore)
    {
        $this->preformatterCore = $preformatterCore;
    }


    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            'preformat' => new Twig_Filter_Method($this, 'preformat'),
        );
    }

    /**
     * Runs code through all preformatters.
     *
     * @param string $content
     *
     * @return string
     */
    public function preformat($content)
    {
        return $this->preformatterCore->preformat($content);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'preformatForTexy_extension';
    }

}
