<?php


namespace Edge\PreformatForTexyBundle;
use Edge\PreformatForTexyBundle\DependencyInjection\Compiler\PreformatterCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * MetaClass for registering bundle.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class EdgePreformatForTexyBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new PreformatterCompilerPass());
    }
}