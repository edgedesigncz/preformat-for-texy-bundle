PreformatForTexyBundle
======================

This bundle provides Twig magro `preformat`, that can trigger some changes on input HTML source code.

Inside, there are several Preformatter, that manipulate with given code. 

Currently defined Preformatters
-------------------------------
- PreCodePreformatter - changes occurences of <pre><code>source</code></pre> in blocks into /--code source \--
- CodePreformatter - changes occurences of <code>source</code> in line into `source`.

Defining new Preformatter
-------------------------
- Preformatter must implement Preformatter\PreformatterInterface.
- Preformatter must be declared as a service with tag called "edge.preformatter". Optional priority might be specified. The higher priority, the sooner the service will be called. Default priority is 0.