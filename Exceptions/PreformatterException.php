<?php


namespace Edge\PreformatForTexyBundle\Exceptions;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreformatterException extends \Exception
{

}