<?php


namespace Edge\PreformatForTexyBundle\Exceptions;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class InvalidArgumentException extends PreformatterException
{

}