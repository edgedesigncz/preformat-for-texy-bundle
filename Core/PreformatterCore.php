<?php


namespace Edge\PreformatForTexyBundle\Core;
use Edge\PreformatForTexyBundle\CommonConstants;
use Edge\PreformatForTexyBundle\Exceptions\InvalidArgumentException;
use Edge\PreformatForTexyBundle\Preformatter\CodePreformatter;
use Edge\PreformatForTexyBundle\Preformatter\PreCodePreformatter;
use Edge\PreformatForTexyBundle\Preformatter\PreformatterInterface;
use FluentDOM;


/**
 * Core class that makes whole preformatting.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreformatterCore 
{
    /** @var \FluentDOM */
    private $fluentDom;

    /** @var string */
    private $charset;

    /** @var PreformatterInterface[] */
    private $preformatters;

    public function __construct(FluentDOM $fluentDom, $charset = 'UTF-8')
    {
        $this->fluentDom = $fluentDom;
        $this->charset = $charset;
        $this->preformatters = array();
    }

    /**
     * Registers new preformater.
     * Given preformatter will be added at the end of the queue.
     *
     * @param PreformatterInterface $preformatter
     */
    public function addPreformatter(PreformatterInterface $preformatter)
    {
        $this->preformatters[] = $preformatter;
    }

    /**
     * Preformat given $source with given $contentType for Texy.
     *
     * @param string $source
     * @param string $contentType
     * @return string
     */
    public function preformat($source, $contentType = 'text/html')
    {
        // load content into FluentDOM
        $sourceForDom = $this->prepareContentForLoad($source);
        $this->fluentDom->load($sourceForDom, $contentType);

        // pass content through preformatters
        $this->runPreformatters();

        $code = $this->extractFluentDomContent();
        return $this->stripCommentMarkers($code);
    }

    /**
     * Loop through all preformatters and make them process the source.
     */
    private function runPreformatters()
    {
        foreach ($this->preformatters as $preformatter)
        {
            $preformatter->preformat($this->fluentDom);
        }
    }

    /**
     * FluentDOM automatically wraps given content into <body> tag.
     * This method extracts data from it.
     */
    private function extractFluentDomContent()
    {
        $domElement = $this->fluentDom->find("//*[name()='body']")[0];

        if ($domElement === null) {
            throw new InvalidArgumentException("Cannot process given code. Please make sure you supplied valid HTML.");
        }

        $html = '';

        $children = $domElement->childNodes;
        foreach ($children as $child) {
            $html .= $child->ownerDocument->saveXML($child);
        }

        return $html;
    }

    /**
     * Prefilters may have added some sections, that needed has been marked to be skipped by DOM tree builder.
     * this method removes those markers.
     *
     * @param $code
     * @return mixed
     */
    private function stripCommentMarkers($code)
    {
        $code = str_replace(CommonConstants::PREFILTER_CODE_COMMENT_START, '', $code);
        return str_replace(CommonConstants::PREFILTER_CODE_COMMENT_END, '', $code);
    }

    /**
     * FluentDOM must have somehow marked charset, in which the content is.
     * We can help him by adding <?xml tag before it.
     *
     * @param string $content
     * @return string
     */
    private function prepareContentForLoad($content)
    {
        return '<?xml version="1.0" encoding="'.$this->charset.'">' . $content;
    }
}