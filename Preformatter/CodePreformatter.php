<?php


namespace Edge\PreformatForTexyBundle\Preformatter;
use DOMElement;
use Edge\PreformatForTexyBundle\CommonConstants;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use FluentDOM;


/**
 * This preformatter replaces all occurences of <code> for `` and html entities for their real meanings.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class CodePreformatter implements PreformatterInterface
{
    const CODE_MARKER_START = '`';

    const CODE_MARKER_END = '`';

    /** @var TexyHelpers */
    private $texyHelpers;

    public function __construct(TexyHelpers $texyHelpers)
    {
        $this->texyHelpers = $texyHelpers;
    }


    /**
     * Extracts all <code> which does not have <pre> as ancestor
     * and transforms them into `contentOfTag`
     * <example>
     *  Transforms
     *      <p> some text <code class="language-php">some code</code></p>
     *  into
     *      <p> some text `some code .[language-php]`</p>
     * </example>
     *
     *
     * @param FluentDOM $content
     * @return void
     */
    public function preformat(FluentDOM & $content)
    {
        $content->find('//code[not(ancestor::pre)]')->replaceWith(function(DOMElement $element){
            $content = $element->ownerDocument->saveXML($element->childNodes->item(0));

            $content = html_entity_decode($content);

            $classModificator = $this->texyHelpers->translateClass($element);

            return self::CODE_MARKER_START . CommonConstants::PREFILTER_CODE_COMMENT_START .
                $content . $classModificator .
                CommonConstants::PREFILTER_CODE_COMMENT_END. self::CODE_MARKER_END;
        });
    }
}