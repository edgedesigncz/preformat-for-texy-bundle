<?php


namespace Edge\PreformatForTexyBundle\Preformatter;
use FluentDOM;


/**
 * Interface, that every preformatter must implement
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
interface PreformatterInterface 
{
    /**
     * Preformat source code in given $content.
     *
     * @param FluentDOM $content
     * @return void
     */
    public function preformat(FluentDOM & $content);
}