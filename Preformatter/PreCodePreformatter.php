<?php


namespace Edge\PreformatForTexyBundle\Preformatter;
use DOMElement;
use Edge\PreformatForTexyBundle\CommonConstants;
use Edge\PreformatForTexyBundle\Helpers\TexyHelpers;
use FluentDOM;

/**
 * This preformatter replaces all occurences of <pre><code></code></pre> for /--code --\ and html entities within for their real meanings.
 * This preformatter must run before CodePreformatter.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PreCodePreformatter implements PreformatterInterface
{
    const LINE_BREAK = "\n";

    const CODE_MARKER_START = '/--code';

    const CODE_MARKER_END = '\--';

    /** @var TexyHelpers */
    private $texyHelpers;

    public function __construct(TexyHelpers $texyHelpers)
    {
        $this->texyHelpers = $texyHelpers;
    }


    /**
     * Replaces <pre><code>something</code></pre> into it's Texy counterpart.
     * <example>
     *  Transforms
     *      <pre>
     *              <code class="language-php">some code</code>
     *      </pre>
     *  into
     *      /--code .[language-php]
     *         some code
     *      \--
     * </example>
     *
     *
     * Preformat source code in given $content.
     *
     * @param FluentDOM $content
     * @return void
     */
    public function preformat(FluentDOM & $content)
    {
        $content->find('//pre/code/..')->replaceWith(function(DOMElement $element){
            $codeElements = $element->getElementsByTagName('code');
            // at least one element is ensured by XPath selector above
            $codeElement = $codeElements->item(0);

            $content = $element->ownerDocument->saveXML($codeElement->childNodes->item(0));

            $classModificator = $this->texyHelpers->translateClass($codeElement);

            return self::LINE_BREAK .
                self::CODE_MARKER_START . $classModificator . self::LINE_BREAK .
                CommonConstants::PREFILTER_CODE_COMMENT_START .
                html_entity_decode($content) .
                CommonConstants::PREFILTER_CODE_COMMENT_END . self::LINE_BREAK .
                self::CODE_MARKER_END . self::LINE_BREAK;
        });


    }
}